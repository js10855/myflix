﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Myflix.Models
{
    public class Movie
    {
        [Key]
        public int MovieID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [ForeignKey("GenreID")]
        public int GenreID { get; set; }        
        public DateTime ReleaseDate { get; set; }
        public Double Duration { get; set; }
        public int Rating { get; set; }    

    }
}
