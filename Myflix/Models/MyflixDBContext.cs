﻿using Microsoft.EntityFrameworkCore;


namespace Myflix.Models
{
    public class MyflixDBContext : DbContext
    {
        public MyflixDBContext(DbContextOptions<MyflixDBContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<Genre> Genre { get; set; }
    }
}
