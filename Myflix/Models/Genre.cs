﻿using System.ComponentModel.DataAnnotations;

namespace Myflix.Models
{
    public class Genre
    {
        [Key]
        public int GenreID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
