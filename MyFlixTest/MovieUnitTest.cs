﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Myflix.Models;
using System.Linq;

namespace MyFlixTest
{
    [TestClass]
    public class MovieUnitTest
    {
        public ServiceCollection Services { get; private set; }
        public ServiceProvider ServiceProvider { get; protected set; }

        [TestInitialize]
        public void Initialize()
        {
            Services = new ServiceCollection();

            Services.AddDbContext<MyflixDBContext>(opt => opt.UseInMemoryDatabase(databaseName: "MyflixDB"),
                ServiceLifetime.Scoped,
                ServiceLifetime.Scoped);

            ServiceProvider = Services.BuildServiceProvider();
        }

        [TestMethod]
        public void TestAddMovie()
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                dbContext.Movies.Add(new Movie { Name = "Harry Potter", Description = "kids",GenreID=1,Duration=3.0,Rating=5 });
                dbContext.Movies.Add(new Movie { Name = "SpiderMan", Description = "action", GenreID = 2, Duration = 3.0, Rating = 4 });
                dbContext.SaveChanges();
                Assert.IsTrue(dbContext.Movies.Count() == 2);
            }
        }

        [TestMethod]
        public void TestGetAllMovie()
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var MovieList = dbContext.Movies.ToList();
                Assert.IsTrue(MovieList.Count == 1);
            }
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestGetMovie(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var Movie = dbContext.Movies.Find(id);
                if (Movie == null)
                {
                    Assert.Fail("Movie not found with id=" + id);
                }
            }
            Assert.IsTrue(true, "Movie got");
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestMovieExists(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var Movie = dbContext.Movies.Find(id);
                if (Movie == null)
                {
                    Assert.Fail("Movie not found with id=" + id);
                }
            }
            Assert.IsTrue(true, "Movie exists for given id");
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestUpdateMovie(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var Movie = dbContext.Movies.Find(id);

                if (Movie == null)
                {
                    Assert.Fail("Movie not found with id=" + id);
                }
                Movie.Description = "new description";
                dbContext.Movies.Update(Movie);
                dbContext.SaveChanges();
                var updatedMovie = dbContext.Movies.Find(id);
                Assert.AreEqual(Movie, updatedMovie);
            }
        }

        [DataRow(1)]
        [DataTestMethod]
        public void TestDeleteMovie(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var originalCount = dbContext.Movies.Count();
                var Movie = dbContext.Movies.Find(id);
                if (Movie == null)
                {
                    Assert.Fail("Movie not found with id=" + id);
                }
                dbContext.Movies.Remove(Movie);
                dbContext.SaveChanges();
                Assert.IsTrue(dbContext.Movies.Count() == originalCount - 1);
            }
        }

        [TestCleanup]
        public virtual void Cleanup()
        {
            ServiceProvider.Dispose();
            ServiceProvider = null;
        }
    }
}
