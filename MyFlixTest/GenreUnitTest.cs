using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Myflix.Models;
using System.Linq;

namespace MyFlixTest
{
    [TestClass]
    public class GenreUnitTest
    {
        public ServiceCollection Services { get; private set; }
        public ServiceProvider ServiceProvider { get; protected set; }

        [TestInitialize]
        public void Initialize()
        {
            Services = new ServiceCollection();

            Services.AddDbContext<MyflixDBContext>(opt => opt.UseInMemoryDatabase(databaseName: "MyflixDB"),
                ServiceLifetime.Scoped,
                ServiceLifetime.Scoped);

            ServiceProvider = Services.BuildServiceProvider();
        }

        [TestMethod]
        public void TestAddGenre()
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                dbContext.Genre.Add(new Genre { Name = "comedy", Description="fun movies" });
                dbContext.Genre.Add(new Genre { Name = "action", Description = "action hungama" });
                dbContext.SaveChanges();
                Assert.IsTrue(dbContext.Genre.Count() == 2);
            }
        }

        [TestMethod]
        public void TestGetAllGenre()
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var genreList = dbContext.Genre.ToList();                
                Assert.IsTrue(genreList.Count == 1);
            }
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestGetGenre(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var genre = dbContext.Genre.Find(id);
                if (genre == null)
                {
                    Assert.Fail("Genre not found with id=" + id);
                }                
            }
            Assert.IsTrue(true, "genre got");
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestGenreExists(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var genre = dbContext.Genre.Find(id);
                if (genre == null)
                {
                    Assert.Fail("Genre not found with id=" + id);
                }
            }
            Assert.IsTrue(true, "genre exists for given id");
        }

        [DataRow(2)]
        [DataTestMethod]
        public void TestUpdateGenre(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var genre = dbContext.Genre.Find(id);
              
                if (genre == null)
                {
                    Assert.Fail("Genre not found with id=" + id);
                }
                genre.Description = "new description";
                dbContext.Genre.Update(genre);
                dbContext.SaveChanges();
                var updatedGenre = dbContext.Genre.Find(id);
                Assert.AreEqual(genre, updatedGenre);
            }
        }

        [DataRow(1)]      
        [DataTestMethod]
        public void TestDeleteGenre(int id)
        {
            using (var dbContext = ServiceProvider.GetService<MyflixDBContext>())
            {
                var originalCount = dbContext.Genre.Count();
                var genre =  dbContext.Genre.Find(id);
                if (genre == null)
                {
                    Assert.Fail("Genre not found with id=" + id);
                }
                dbContext.Genre.Remove(genre);
                dbContext.SaveChanges();
                Assert.IsTrue(dbContext.Genre.Count() == originalCount-1);
            }
        }

        [TestCleanup]
        public virtual void Cleanup()
        {
            ServiceProvider.Dispose();
            ServiceProvider = null;
        }
    }
}
